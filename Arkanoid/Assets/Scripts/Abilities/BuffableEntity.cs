﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuffableEntity : MonoBehaviour
{
    private readonly Dictionary<AScriptableBuff, ATimedBuff> _buffs = new Dictionary<AScriptableBuff, ATimedBuff>();

    private void Update()
    {
        foreach(var buff in _buffs.Values.ToList())
        {
            buff.Tick(Time.deltaTime);
            if(buff.IsFinished)
            {
                _buffs.Remove(buff.Buff);
            }
        }
    }

    public void AddBuff(ATimedBuff buff)
    {
        if(_buffs.ContainsKey(buff.Buff))
        {
            _buffs[buff.Buff].Activate();
        }
        else
        {
            _buffs.Add(buff.Buff, buff);
            buff.Activate();
        }
    }
}
