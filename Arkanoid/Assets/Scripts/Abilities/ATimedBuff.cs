﻿using UnityEngine;

public abstract class ATimedBuff
{
    float Duration;
    protected readonly GameObject Obj;
    public AScriptableBuff Buff;
    public bool IsFinished;
    public ATimedBuff(AScriptableBuff buff, GameObject obj)
    {
        Buff = buff;
        Obj = obj;
    }

    public void Tick(float delta)
    {
        Duration -= delta;
        if(Duration <= 0)
        {
            End();
            IsFinished = true;
        }
    }
    
    public void Activate()
    {
        if(Duration <= 0)
        {
            ApplyEffect();
        }
        if(Buff.IsDurationStacked || Duration <= 0)
        {
           Duration += Buff.Duration;
        }
    }
    protected abstract void ApplyEffect();
    public abstract void End();
}
