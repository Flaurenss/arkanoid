using UnityEngine;

public abstract class AScriptableBuff : ScriptableObject
{
    public float Duration;
    public bool IsDurationStacked;
    public abstract ATimedBuff InitializeBuff(GameObject obj);
}