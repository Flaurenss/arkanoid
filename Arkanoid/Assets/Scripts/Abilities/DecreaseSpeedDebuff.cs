using UnityEngine;

[CreateAssetMenu(menuName = "Debuff/SpeedDebuff")]
public class DecreaseSpeedBuff : AScriptableBuff
{
    public float speedDecrease;
    public override ATimedBuff InitializeBuff(GameObject obj)
    {
        return new TimedDecreaseSpeedBuff(this, obj);
    }
}