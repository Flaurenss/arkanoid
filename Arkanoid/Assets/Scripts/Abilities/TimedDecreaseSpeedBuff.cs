using UnityEngine;

public class TimedDecreaseSpeedBuff : ATimedBuff
{
    private readonly PaddleController _paddleController;
    public TimedDecreaseSpeedBuff(AScriptableBuff buff, GameObject obj) : base(buff, obj)
    {
        _paddleController = obj.GetComponent<PaddleController>();   
    }

    protected override void ApplyEffect()
    {
        DecreaseSpeedBuff speedBuff = (DecreaseSpeedBuff) Buff;
        _paddleController.paddleSpeed -= speedBuff.speedDecrease;
    }

    public override void End()
    {
        DecreaseSpeedBuff speedBuff = (DecreaseSpeedBuff) Buff;
        _paddleController.paddleSpeed += speedBuff.speedDecrease;
    }
}