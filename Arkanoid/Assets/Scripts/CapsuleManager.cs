﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CapsuleManager", menuName = "ScriptableObjects/CapsuleManager", order = 1)]
public class CapsuleManager : ScriptableObject
{
    public List<Capsule> AvailableBuffs;
    public List<Capsule> AvailableDebuffs;
    [Range(0,100)]
    public float BuffProbability;
    [Range(0,100)]
    public float DebuffProbability;
}
