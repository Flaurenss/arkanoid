﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Capsule : MonoBehaviour
{
    private void FixedUpdate()
    {
        transform.Translate(Vector3.down * Time.fixedDeltaTime);    
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Paddle")
        {
            ApplyEffect();
            Destroy(gameObject);
        }
        else if(other.tag == "DeadZone")
        {
            Destroy(gameObject);
        }
    }
    protected abstract void ApplyEffect();
}
