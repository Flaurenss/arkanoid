﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PaddleController))]
public class InputController : MonoBehaviour
{
    private PaddleController paddleController;
    private void Awake()
    {
        paddleController = GetComponent<PaddleController>(); 
    }

    private void Update()
    {
        var playerInput = Input.GetAxis("Horizontal");
        paddleController.PaddleMovement(playerInput);
    }
}
