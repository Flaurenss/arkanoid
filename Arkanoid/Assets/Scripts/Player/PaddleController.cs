﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public bool paddleTransforming {private set; get;}
    public GameObject LeftWallLimit;
    public GameObject RightWallLimit;
    public float paddleSpeed{get;set;} = 10f;

    public bool debuffActive {private set; get;}
    public bool buffActive {private set; get;}
    private float paddleTransformDuration = 3f;
    private float _paddleSpeed;
    private float displacement;
    private Vector2 colliderSize;
    private BoxCollider2D playerCollider;
    private float xInitialPos;
    private float rightLimit;
    private float leftLimit;
    private float originalPaddleWidth;
    private SpriteRenderer sprite;
    Vector2 velocity;

    private void Start() 
    {
        buffActive = false;
        paddleTransforming = false;
        sprite = GetComponent<SpriteRenderer>();
        originalPaddleWidth = sprite.size.x;
        _paddleSpeed = paddleSpeed;
        xInitialPos = transform.position.x;
        playerCollider = GetComponent<BoxCollider2D>();
        CheckColliderSize();
    }
    public void PaddleMovement(float playerInput)
    {
        if(playerInput != 0 && GameManager.CanPlay)
        {
            velocity.x = playerInput * paddleSpeed;
            velocity.y = 0;
            transform.Translate(velocity * Time.deltaTime);
            var tmpPos = transform.position;
            tmpPos.x = Mathf.Clamp(tmpPos.x, leftLimit, rightLimit);
            transform.position = tmpPos;
        }
    }
    /// <summary>
    /// Check the actual player collider size and updates left and right limits
    /// </summary>
    void CheckColliderSize()
    {
        colliderSize = playerCollider.size;
        rightLimit = RightWallLimit.transform.position.x - (colliderSize.x/2);
        leftLimit = LeftWallLimit.transform.position.x + (colliderSize.x/2);
    }
    public Vector2 GetIdealBallPosition()
    {   
        Vector2 idealPos = new Vector2(transform.position.x, transform.position.y + colliderSize.y);
        return idealPos;
    }
    /// <summary>
    /// Decrease paddle speed temporary as a debuff
    /// </summary>
    /// <param name="decreaseSpeed"></param>
    public void DecreaseSpeed(float decreaseSpeed)
    {
        debuffActive = true;
        paddleSpeed -= decreaseSpeed;
        StartCoroutine(Counter());
    }
private void Update() {
    if(Input.GetKey(KeyCode.T))
    {
        PaddleWidthTransformation(3);
    }
    if(buffActive)
    {
        CheckColliderSize();
    }
}
    public void PaddleWidthTransformation(float increaseWidth)
    {
        buffActive = true;
        StartCoroutine(AnimatePaddleWidth(increaseWidth));
    }

    IEnumerator AnimatePaddleWidth(float width)
    {
        Debug.Log("Entra en corotuina");
        paddleTransforming = true;
        StartCoroutine(ResetPaddleWidth(paddleTransformDuration));
        if(width > sprite.size.x)
        {
            //We are expanding
            float currentWidth = sprite.size.x;
            while(currentWidth < width)
            {
                currentWidth += Time.deltaTime * 2;
                sprite.size = new Vector2(currentWidth,sprite.size.y);
                playerCollider.size = new Vector2(currentWidth, colliderSize.y);
                yield return null;
            }
        }
        else
        {
            //We are returning to original size
            float currentWidth = sprite.size.x;
            while(currentWidth > width)
            {
                currentWidth -= Time.deltaTime * 2;
                sprite.size = new Vector2(currentWidth,sprite.size.y);
                playerCollider.size = new Vector2(currentWidth, colliderSize.y);
                yield return null;
            }
            paddleTransforming = false;
        }
        // paddleTransforming = false;
    }

    IEnumerator ResetPaddleWidth(float duration)
    {
        //Wait for duration in order to reset paddlewidth
        yield return new WaitForSeconds(duration);
        if(paddleTransforming)
        {
            PaddleWidthTransformation(originalPaddleWidth);
        }
        else{
            buffActive = false;
        }
    }

    IEnumerator Counter()
    {
        float timer = 0; 
        float duration = 10;
        while(timer <= duration)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        paddleSpeed = _paddleSpeed;
        debuffActive = false;
    }
}
