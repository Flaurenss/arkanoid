﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    private int playerLives = 3;
    private int totalLives = 3;
    private int currentStage = 0;
    [SerializeField] private GameObject gameOverText;
    [SerializeField] private GameObject victoryText;
    [SerializeField] public TextMeshProUGUI stageText;
    [SerializeField] private GameObject[] stageContainer;
    [SerializeField] private GameObject[] livesContainer;
    private GameState gameState;
    private int numBricksStage;
    private int numBricksStageDeleted = 0;
    private int maxStages;
    public static bool CanPlay{get; private set;}
    public delegate void BallManagement();
    public static BallManagement BallReset;
    public static BallManagement ChangeBallSpeed;
    private void Start()
    {
        stageText.text = "STAGE 1";
        maxStages = stageContainer.Length;
        gameState = GameState.Game;
        CalculateBriks(stageContainer[currentStage]);
        Brick.BrickDestroyed += BrickDeleted;
    }
    private void Update() 
    {
        CheckGameState();
        if(Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("MainScene");
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void CalculateBriks(GameObject parentBricks)
    {
        var totalBricks = parentBricks.transform.GetComponentsInChildren<Transform>();
        foreach(var brick in totalBricks)
        {
            if(brick.gameObject.CompareTag("Block"))
            {
                var brickType = brick.gameObject.GetComponent<Brick>().brickType;
                if(brick.gameObject.GetComponent<Brick>().brickType != BrickType.Invincible)
                {
                   numBricksStage ++; 
                }
            }
        }
    }
    private void CheckGameState()
    {
        switch(gameState)
        {
            case GameState.Menu:
            CanPlay = false;
            break;
            case GameState.Game:
            CanPlay = true;
            break;
            case GameState.GameOver:
            CanPlay = false;
            break;
            case GameState.EndGame:
            CanPlay = false;
            victoryText.SetActive(true);
            break;
        }
    }
    /// <summary>
    /// When the ball hits the dead zone, rest a life 
    /// </summary>
    public void BallHitDeadZone()
    {
        if(gameState == GameState.Game)
        {
            playerLives --;
            //Decrease life indicator
            livesContainer[(totalLives-playerLives) - 1].gameObject.SetActive(false);
            BallReset();
            if(playerLives == 0)
            {
                gameState = GameState.GameOver;
                gameOverText.SetActive(true);
            }
        }
    }
    public void BrickDeleted()
    {
        numBricksStageDeleted ++;
        if(numBricksStageDeleted == numBricksStage)
        {
            BallReset();
            NextStage();
            numBricksStageDeleted = 0;
        }
    }
    private void NextStage()
    {
        currentStage ++;
        if(currentStage < maxStages)
        {
            ChangeBallSpeed();
            CheckLevelToLoad();
            stageText.text = "STAGE " + (currentStage + 1);
        }
        else
        {
            gameState = GameState.EndGame;
        }
    }
    private void CheckLevelToLoad()
    {
        //Hide completed stage
        stageContainer[currentStage-1].gameObject.SetActive(false);
        //Show new stage
        CalculateBriks(stageContainer[currentStage]);
        stageContainer[currentStage].gameObject.SetActive(true);
    }
    private enum GameState
    {
        Menu,
        Game, 
        GameOver,
        EndGame
    }
}
