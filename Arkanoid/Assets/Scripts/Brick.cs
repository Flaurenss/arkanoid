﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [SerializeField] private BrickType _brickType;
    [SerializeField] private int maxHits;
    [SerializeField] private CapsuleManager capsuleManager;
    [SerializeField] private GameManager gameManager;
    public GameObject CapsulePlus;
    public GameObject CapsuleMinus;
    public BrickType brickType{get; private set;}
    private int hitsRecived = 0;
    private Animator brickAnimator;  
    private bool destroyed = false;

    public delegate void BrickBehaviour();
    public static BrickBehaviour BrickDestroyed;

    private void Awake() {
        brickType = _brickType;
    }

    private void Start()
    {
        brickAnimator = GetComponent<Animator>();    
    }
    public void RecieveHit()
    {
        switch (_brickType)
        {
            case BrickType.Invincible:
            brickAnimator.SetTrigger("Hit");
            break;
            case BrickType.Easy:
                UpdateHitsCounter();
            break;
            case BrickType.Medium:
                UpdateHitsCounter();
            break;
            case BrickType.Hard:
                UpdateHitsCounter();
            break;
        }
    }

    private void UpdateHitsCounter()
    {
        hitsRecived++;
        if(hitsRecived != maxHits && !destroyed)
        {
            brickAnimator.SetTrigger("Hit");
        }
        else
        {
            destroyed = true;
            Destroy(gameObject);
            BrickDestroyed();
            CapsuleSpawn();
        }
    }

    private void CapsuleSpawn()
    {
        var buffSpawnChange = Random.Range(0,100f);
        var debuffSpawnChange = Random.Range(0,100f);
        var alreadySpawn = false;
        if(buffSpawnChange <= capsuleManager.BuffProbability)
        {
            alreadySpawn = true;
            SpawnCapsule(true);
        }
        if(debuffSpawnChange <= capsuleManager.DebuffProbability && !alreadySpawn)
        {
            SpawnCapsule(false);
        }
    }

    private Capsule SpawnCapsule(bool isBuff)
    {
        List<Capsule> collection;
        if(isBuff)
        {
            collection = capsuleManager.AvailableBuffs;
        }
        else
        {
            collection = capsuleManager.AvailableDebuffs;
        }
        int rndIndex = Random.Range(0, collection.Count);
        var prefab = collection[rndIndex];
        Capsule newCapsule = Instantiate(prefab, transform.position, Quaternion.identity) as Capsule;
        return null;
    }
}
    public enum BrickType
    {
        Invincible,
        Easy,
        Medium,
        Hard
    }