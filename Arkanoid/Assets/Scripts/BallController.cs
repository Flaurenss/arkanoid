﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BallController : MonoBehaviour
{
    public AudioClip paddleCollisionAudio;
    public AudioClip deadCollisionAudio;
    public AudioClip brickCollisionAudio;
    [SerializeField, Range(0f, 7f)] private float ballSpeed = 7f;
    [SerializeField] private GameManager gameManager;
    private float _initialBallSpeed;
    [SerializeField] private LayerMask myLayerMask;
    [SerializeField] private PaddleController paddleController;
    private Vector3 displacement;
    //Last direction of the ball
    private Vector3 direction;
    private bool ballInGame = false;
    private CircleCollider2D ballCollider;
    private Vector3 saveNormal;
    private RaycastHit2D lastHitSave = new RaycastHit2D();
    Vector3 destiny;
    public delegate void BallChecker();
    public static BallChecker Dead;
    private GameObject parent;
    private float correctAngle;
    private bool isPaddleLast = false;
    private AudioSource audioSource;

    private void OnDestroy() 
    {
        GameManager.ChangeBallSpeed -= IncreaseSpeed;
        GameManager.BallReset -= BallReset;
    }
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        _initialBallSpeed = ballSpeed;
        GameManager.BallReset += BallReset;
        GameManager.ChangeBallSpeed += IncreaseSpeed;
        parent = transform.parent.gameObject;
        ballCollider = GetComponent<CircleCollider2D>();
    }
    void Update()
    {
        if(Input.GetKeyDown("space") && !ballInGame && GameManager.CanPlay)
        {
            ballInGame = true;
            ShootBall();
        }
        DrawRays();
    }
    private void BallReset()
    {
        displacement = new Vector3();
        direction = new Vector3();
        transform.SetParent(parent.transform);
        transform.position = paddleController.GetIdealBallPosition();
        ballInGame = false;
    }
    private void IncreaseSpeed()
    {
        ballSpeed++;
    }
    private void DrawRays()
    {
        var normalRay = ballCollider.radius + 0.04f;
        var rayLength = ballCollider.radius + 1.05f;
        //diagonal
        // Debug.DrawRay(transform.position, direction * rayLength, Color.black);
        // up
        // Debug.DrawRay(transform.position, Vector3.up * Mathf.Sign(direction.y) * rayLength, Color.black);
        // down
        // Debug.DrawRay(transform.position, Vector3.right * Mathf.Sign(direction.x) * rayLength, Color.black);

        //Normal rays
        //diagonal
        Debug.DrawRay(transform.position, direction * normalRay, Color.black);
        //up
        Debug.DrawRay(transform.position, Vector3.up * Mathf.Sign(direction.y) * normalRay, Color.red);
        //down
        Debug.DrawRay(transform.position, Vector3.right * Mathf.Sign(direction.x) * normalRay, Color.red);
        Debug.DrawRay(transform.position, Vector3.left * Mathf.Sign(direction.x) * normalRay, Color.red);
    }

    private void FixedUpdate()
    {
        CheckCollisions();
        MoveBall();
    }
    private void CheckCollisions()
    {
        GetTrueNormal();
        var collection = CheckSphereCast();
        var hitCollider = CheckRayCast();
        CheckNearCollisions(hitCollider, collection);
    }
    private void GetTrueNormal()
    {
        var rayDistance = ballCollider.radius + 1f;
        var up = Physics2D.Raycast(transform.position, Vector3.up * Mathf.Sign(direction.y), rayDistance + 0.05f,myLayerMask);
        var right = Physics2D.Raycast(transform.position, Vector3.right * Mathf.Sign(direction.x), rayDistance + 0.05f,myLayerMask);
        var diagonal = Physics2D.Raycast(transform.position, direction, rayDistance ,myLayerMask);
        if(diagonal.collider)
        {
            if(diagonal.normal.x == 0 || diagonal.normal.y == 0)
            {
                correctAngle = Vector3.Angle(direction, diagonal.normal);
                lastHitSave = diagonal;
                saveNormal = diagonal.normal;
            }
        }
        else if(right.collider)
        {
            if(right.normal.x == 0 || right.normal.y == 0)
            {
                correctAngle = Vector3.Angle(direction, right.normal);
                lastHitSave = right;
                saveNormal = right.normal;
            }
        }
        else if(up.collider)
        {
            if(up.normal.x == 0 || up.normal.y == 0)
            {
                correctAngle = Vector3.Angle(direction, up.normal);
                lastHitSave = up;
                saveNormal = up.normal;
            }
        }
    }
    private RaycastHit2D CheckRayCast()
    {
        RaycastHit2D hitReturn = new RaycastHit2D();
        var rayDistance = ballCollider.radius + 0.04f;
        var up = Physics2D.Raycast(transform.position, Vector3.up * Mathf.Sign(direction.y), rayDistance,myLayerMask);
        var right = Physics2D.Raycast(transform.position, Vector3.right * Mathf.Sign(direction.x), rayDistance ,myLayerMask);
        var diagonal = Physics2D.Raycast(transform.position, direction, rayDistance,myLayerMask);
        if(diagonal.collider)
        {
            return diagonal;
        }
        else if(up.collider)
        {
            return up;
        }
        else if(right.collider)
        {
            return right;
        }
        else{
            return hitReturn;
        }
    }
    private RaycastHit2D[] CheckSphereCast()
    {
        RaycastHit2D[] isHit = Physics2D.CircleCastAll(transform.position, ballCollider.radius + 0.005f, direction,0.05f,myLayerMask);
        return isHit;
    }
    private void CheckNearCollisions(RaycastHit2D hit, RaycastHit2D[] collection)
    {
        if(hit.collider && !hit.collider.CompareTag("Capsule"))
        {   
            //First check if we hit the dead zone
            CheckDeadZone(hit);
            var hitPaddle = CheckPaddleCollision(hit);
            //Check if the last hit and the actual was the paddle in order to avoid jitter issues
            if(isPaddleLast && !hitPaddle || hitPaddle && !isPaddleLast || !hitPaddle && !isPaddleLast)
            {
                if(hitPaddle)
                {
                    isPaddleLast = true;
                    var x = HitFactor(transform.position, hit.transform.position, hit.collider.bounds.size.x);
                    direction = new Vector3(x, 1);
                    audioSource.clip = paddleCollisionAudio;
                    audioSource.Play();
                }
                else if(hit.normal.x == 0 || hit.normal.y == 0)
                {
                    PlayBrickAudio(hit);
                    isPaddleLast = false;
                    destiny = hit.point;
                    Debug.DrawRay(destiny, hit.normal, Color.black, 1f);
                    
                    direction = Vector3.Reflect(direction, hit.normal);
                    Debug.DrawRay(hit.point, direction, Color.red, 1f);
                }
                else
                {
                    PlayBrickAudio(lastHitSave);
                    destiny = lastHitSave.point;
                    Debug.DrawRay(destiny, saveNormal, Color.green, 1f);
                    direction = Vector3.Reflect(direction, saveNormal);
                }
                //Check how many blocks has the ball hit to destroy
                if(collection.Length > 0)
                {
                    foreach(var col in collection){
                        if(col.transform.gameObject.CompareTag("Block"))
                        {
                            col.transform.gameObject.GetComponent<Brick>().RecieveHit();
                        }
                    }
                }
                else if(lastHitSave.collider.gameObject.CompareTag("Block"))
                {
                    lastHitSave.transform.gameObject.GetComponent<Brick>().RecieveHit();
                }
                SetDisplacement(direction.normalized);
            }
        }
    }

    void PlayBrickAudio(RaycastHit2D hitCollider)
    {
        if(hitCollider.collider.tag != "DeadZone")
        {
            audioSource.clip = brickCollisionAudio;
            audioSource.Play();    
        }
    }
    private float HitFactor(Vector2 ballPos, Vector2 paddlePos, float paddleWidth)
    {
        return (ballPos.x - paddlePos.x) / paddleWidth;
    }
    private bool CheckPaddleCollision(RaycastHit2D collision)
    {
        bool result = false;
        if(collision.collider.gameObject.CompareTag("Paddle"))
        {
            result = true;
        }
        return result;
    }
    private void CheckDeadZone(RaycastHit2D hit)
    {
        if(hit.collider.gameObject.CompareTag("DeadZone"))
        {
            audioSource.clip = deadCollisionAudio;
            audioSource.Play();
            gameManager.BallHitDeadZone();
        }
    }
    void ShootBall()
    {
        transform.parent = null;
        audioSource.clip = paddleCollisionAudio;
        audioSource.Play();
        direction = new Vector3(1,1);
        direction = direction.normalized;
        SetDisplacement(direction.normalized);
    }

    void MoveBall()
    {
        if(transform.position != new Vector3(destiny.x, destiny.y))
        {
            transform.Translate(displacement);
        }
    }
    void SetDisplacement(Vector3 dir)
    {
        displacement = dir * ballSpeed * Time.deltaTime;
    }

    private void OnDrawGizmos() {
        Gizmos.DrawSphere(transform.position + direction * 0.05f , GetComponent<CircleCollider2D>().radius + 0.005f);
    }
    
}
